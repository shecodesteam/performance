import React,  { memo } from "react";


const Child = (props) => {


    function changeMyName(e){
        props.changeName(e.target.value);

    }
    console.log(`child name : ${props.name}`);
    return (
        <div>
            <h1>
            {props.name}
            </h1>
            <input type="text" onChange={changeMyName}>
            </input>
           
        </div>
    );
};
export default memo(Child);