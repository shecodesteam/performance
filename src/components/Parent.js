import React, {useState, useCallback, useEffect, useMemo} from 'react';
import Child from "./Child";


function Parent(){
   
    const [localNumber, setLocalNumber] = useState(0);
    const [childName, setChildName] = useState("Ricky");
    const [newName, setNewName] = useState('');
    const [names, setNames] = useState(["Michael","Ron", "Irit","Moshe", "Hagit", "Noam", "Ortal"]);
    const memoizesCallback = useCallback((name) => changeChildName(name),[])
    const memoizedValue = useMemo(()=> getLongestName(), [names])

    function incrementLocal(){
        setLocalNumber(state=>state+1);
    }
    function changeChildName(name){
        setChildName(name);
    }
    function addNewName(e){
        e.preventDefault();
        setNewName(e.target.value);
    }
    function mySubmitHandler(e){
        e.preventDefault();
        let namesArray = [...names];
        namesArray.push(newName);
        setNames(namesArray);
    }
    function getLongestName(){
      
        let namesArray = [...names];
        return namesArray.reduce(function (a, b) { return a.length > b.length ? a : b });
    }
    return(
        <div style={{marginTop:'20px',marginBottom:'20px' }}>


            <Child name={childName} changeName={memoizesCallback} />
            <button onClick={incrementLocal}>Click increment parent</button>
            <h1>local number: {localNumber}</h1>
            <form onSubmit={mySubmitHandler}>
                <p>Enter new name, and submit:</p>
                <input type="text" onChange={addNewName}>
                </input>
                <input
                    type='submit'
                />
            </form>
            <p>The longest name is: {memoizedValue}</p>
  
        </div>
    )

}
export default Parent;